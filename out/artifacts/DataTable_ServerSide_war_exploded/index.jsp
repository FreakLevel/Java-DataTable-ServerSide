<%--
  Created by IntelliJ IDEA.
  User: FreakLevel
  Date: 8/24/2018
  Time: 1:18 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>$Title$</title>
      <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
      <script src="http://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  </head>
  <body>

  <table id="example" class="display" style="width:100%">
      <thead>
      <tr>
          <th>Name</th>
          <th>Position</th>
          <th>Office</th>
          <th>Age</th>
          <th>Start date</th>
          <th>Salary</th>
      </tr>
      </thead>
  </table>

  <script>
      $(document).ready(function() {
          $('#example').DataTable({
              'processing': true,
              'serverSide': true,
              'ajax': {
                  'url': '/Servlet',
                  'type': 'GET'
              }
          });
      } );
  </script>
  </body>
</html>
