import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "Servlet", urlPatterns = {"/Servlet", "/root/ruta"})
public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        /*String json = "{"+
                "\"draw\": 1,"+
                "\"recordsTotal\": 3,"+
                "\"recordsFiltered\": 3,"+
                "\"data\": ["+
                "["+
                "\"Airi Satou\","+
                "\"Accountant\","+
                "\"Tokyo\","+
                "\"20\","+
                "\"28th Nov 08\","+
                "\"$162,700\""+
                "],"+
                "["+
                "\"Angelica Ramos\","+
                "\"Chief Executive Officer (CEO)\","+
                "\"London\","+
                "\"30\","+
                "\"9th Oct 09\","+
                "\"$1,200,000\""+
                "],"+
                "["+
                "\"Ashton Cox\","+
                "\"Junior Technical Author\","+
                "\"San Francisco\","+
                "\"25\","+
                "\"12th Jan 09\","+
                "\"$86,000\""+
                "]"+
                "]"+
                "}";*/

        User usuario = new User();

        int draw = Integer.parseInt(request.getParameter("draw"));
        int offset = Integer.parseInt(request.getParameter("start"));
        int limit = Integer.parseInt(request.getParameter("length"));

        List<User> users = usuario.PaginationUsers(offset, limit);
        int total = usuario.CountTable();

        String data = "";

        for (User usu : users) {
            data += "["+
                    "\""+usu.name+"\","+
                    "\"Accountant\","+
                    "\"Tokyo\","+
                    "\"20\","+
                    "\"28th Nov 08\","+
                    "\"$162,700\""+
                    "],";
        }

        data = data.substring(0, data.length()-1);

        String json = "{"+
                "\"draw\": "+draw+","+
                "\"recordsTotal\": "+total+","+
                "\"recordsFiltered\": "+total+","+
                "\"data\": ["+data+ "]"+
                "}";

        String value = request.getParameter("draw");
        String value2 = request.getParameter("start");
        String value3 = request.getParameter("length");

        response.setStatus(200);
        response.getWriter().write(json);
    }
}
