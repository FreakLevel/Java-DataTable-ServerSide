import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class User {

    public String name;

    public User(){
    }

    public List<User> GetAllUsers(){
        String query = String.format("SELECT * FROM public.\"user\"");
        List userList = new ArrayList();
        try {
            userList = CreateList(query);
        }catch (Exception ex){
            ex.printStackTrace();
            System.out.println(ex.getMessage());
        }

        return userList;
    }

    public List<User> PaginationUsers(int offset, int limit){
        String query = String.format("SELECT * FROM public.\"user\" limit %d offset %d", limit, offset);
        List userList = new ArrayList();
        try {
            userList = CreateList(query);
        }catch (Exception ex){
            ex.printStackTrace();
            System.out.println(ex.getMessage());
        }

        return userList;
    }

    public List<User> CreateList(String query){
        List userList = new ArrayList();
        try {
            ResultSet rs = new Connect().Connection().createStatement().executeQuery(query);

            while (rs.next()){
                User us = new User();
                us.name = rs.getString("name");
                userList.add(us);
            }

        }catch (SQLException ex){
            ex.printStackTrace();
            System.out.println(ex.getMessage());
        }

        return userList;
    }

    public int CountTable(){
        String query = String.format("SELECT count(id) FROM public.\"user\"");
        int count = 0;
        try {
            ResultSet rs = new Connect().Connection().createStatement().executeQuery(query);

            while (rs.next()){
                count = rs.getInt(1);
            }

        }catch (SQLException ex){
            ex.printStackTrace();
            System.out.println(ex.getMessage());
        }

        return count;
    }


}
