import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Connection;

import org.postgresql.*;

public class Connect {

    private final String url = "jdbc:postgresql://localhost/datatable";
    private final String user = "postgres";
    private final String pass = "pgAdmin";

    public Connection Connection(){
        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection(url, user, pass);

        }catch (ClassNotFoundException | SQLException ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }

        return con;
    }
}
